import { Component, AfterViewInit, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { Image } from '../interfaces/image.interface';

import { Slide } from './slide.model';

import {Observable} from 'rxjs/Rx';


@Component({
    selector: 'slider',
    templateUrl: './slider.component.html',
    styleUrls : ['./slider.component.css'],
    /* Test animation TODO : DELETE
    animations: [
        trigger('slide', [
            state('inactive', style({
                top: '-200%',
                position:'absolute',
                width: "100%"

            })),
            state('active', style({
                top:'0',
                "z-index": 1
            })),
            state('previous', style({
                top:'0',
            })),
            transition('inactive => active', [
                animate(700, keyframes([
                    style({top: '-200%', offset: 0}),
                    style({top: 0, offset: 1})
                ]))
            ])
        ])
    ]*/
})

export class Slider implements AfterViewInit{

    images : Image[]= [
        { "title": "Lorem Ipsum", "text": "Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor", "url":"#",  "src": "../assets/img/slide-image-1.jpg" },
        { "title": "Lorem Ipsum", "text": "Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor", "url":"#",  "src": "../assets/img/slide-image-2.jpg" },
        { "title": "Lorem Ipsum", "text": "Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor", "url":"#",  "src": "../assets/img/slide-image-3.jpg" }
    ];

    interval: any;
    timeout: any;

    config:{
        duration: number;
        autoSlide: boolean;
    };

    duration = 5000; autoSlide = true;
    progressBarWitdh:number = 0;

    slides_array: Slide[];

    animating: boolean;
    pages: number; //pagger
    count: number=1;

    constructor(){
        this.slides_array = [];

        //Initialisation de la config
        this.config = {
            duration: this.duration,
            autoSlide: this.autoSlide
        }


        //Assignation du tableau de slide (slide model array)
        for(let i =0; i < this.images.length; i++){
            this.slides_array[i] = new Slide(this.images[i]);
        }

        if(this.config.autoSlide){
            let interval = Observable.interval(this.config.duration).subscribe(()=>{ this.next(false) } );
            this.interval = interval;
        }

        //Initialisation du slider
        this.slides_array[this.slides_array.length -1].addClass('previous');
        this.slides_array[0].addClass("active"); 
        this.pages = this.slides_array.length;

        this.initPager();

    }

    initPager(){
        for(let i = 0; i < this.slides_array.length; i++){
            this.slides_array[i].page = i+1;
        }
    }


    animatingToFalse(){
        this.animating = false;
    }

    unsubscribe(bool = false){
        if(bool){
            this.interval.unsubscribe();
            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                let interval = Observable.interval(5000).subscribe(()=>{ this.next(false); } );
                this.interval = interval;
            }, 5000);
        }
    }

    prev(unsubscribe = false){
        let countSlide = this.slides_array.length; // Nombre de slide in slider
        this.unsubscribe(unsubscribe);
        this.slides_array.forEach((slide) => { // parcours les slides en retirant la classe previous
            slide.removeClass("previous");
        });
        if(this.slides_array[0].hasClass("active")){
            this.slides_array[0].removeClass("active");
            this.slides_array[0].addClass("previous");
            this.slides_array[countSlide-1].addClass("active");
            this.count = this.slides_array[countSlide-1].page
        }
        else{
            for(let i = countSlide-1; i >= 0; i--){
                if(this.slides_array[i].hasClass("active")){ // dans toutes les slides, je trouve celle qui a la classe active
                    this.slides_array[i].removeClass("active"); // lui retire la classe active
                    this.slides_array[i].addClass("previous"); // lui rajoute la classe previous
                    this.slides_array[i-1].addClass('active'); // Ajoute la classe active à la slide suivante
                    this.count = this.slides_array[i-1].page;
                    break;
                }
            }
        }
        this.progressBar();
    }


    next(unsubscribe = false){
        let countSlide = this.slides_array.length; // Nombre de slide in slider
        this.unsubscribe(unsubscribe); // désactive le slide auto pour 5 secondes
        this.slides_array.forEach((slide) => { // parcours les slides en retirant la class previous
            slide.removeClass("previous");
        });

        if(this.slides_array[countSlide - 1].hasClass("active")){ //si je suis à la dernière slide
            this.slides_array[countSlide - 1].addClass("previous"); // je lui met la classe previsous
            this.slides_array[countSlide - 1].removeClass("active"); //lui retire la classe active
            this.slides_array[0].addClass("active"); // puis rajoute la class active à la première slide
            this.count = this.slides_array[0].page;
        }
        else{
            for(let i = 0; i < countSlide; i++){
                if(this.slides_array[i].hasClass("active")){ // dans toutes les slides, je trouve celle qui a la classe active
                    this.slides_array[i].removeClass("active"); // lui retire la classe active
                    this.slides_array[i].addClass("previous"); // lui rajoute la classe previous
                    this.slides_array[i+1].addClass('active'); // Ajoute la classe active à la slide suivante
                    this.count = this.slides_array[i+1].page;
                    break;
                }
            }
        }
        this.progressBar();
    }


    progressBar(){
        let countSlide = this.slides_array.length;
        for(let i = 0; i < countSlide; i++){
            if(this.slides_array[i].hasClass("active")){ 
                this.progressBarWitdh =  (i+1)*100/(countSlide) ;
                console.log(this.progressBarWitdh);
                break;
            }
        }
    }


    ngAfterViewInit() {
        this.progressBar();
    }


    ngOnDestroy(){
        this.interval.unsubscribe();
    }


}

