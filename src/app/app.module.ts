import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Slider } from './slider/slider.component';
import { Slider2 } from './slider2/slider.component'

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    Slider, 
    Slider2
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
