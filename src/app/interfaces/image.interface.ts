
//image contract
export interface Image{
    title: string;
    text: string;
    src: string;
    url: string;
}