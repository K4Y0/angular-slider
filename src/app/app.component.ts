import { Component } from '@angular/core';

import { Slider } from './slider/slider.component';
import { Slider2 } from './slider2/slider.component';

import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser'
import { Image } from './interfaces/image.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'app works!';

}
