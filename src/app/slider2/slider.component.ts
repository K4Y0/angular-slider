import { Component, AfterViewInit, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { Image } from '../interfaces/image.interface';

import { Slide } from './slide.model';

import {Observable} from 'rxjs/Rx';


@Component({
    selector: 'slider-app',
    templateUrl: './slider.component.html',
    styleUrls : ['./slider.component.css'],
    animations: [
        trigger('anim', [
            state('*', style({
                transitionDuration : "700ms"
            }))
        ])
    ]
})

export class Slider2 implements AfterViewInit{

    images : Image[]= [
        { "title": "Lorem Ipsum", "text": "Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor", "url":"#",  "src": "../assets/img/slide-image-1.jpg" },
        { "title": "Lorem Ipsum", "text": "Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor", "url":"#",  "src": "../assets/img/slide-image-2.jpg" },
        { "title": "Lorem Ipsum", "text": "Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor", "url":"#",  "src": "../assets/img/slide-image-3.jpg" }
    ];

    interval: any;
    timeout: any;

    config:{
        duration: number;
        autoSlide: boolean;
    };

    duration = 5000; autoSlide = true;
    progressBarWitdh:number = 0;

    slides_array: Slide[];

    animating: boolean;
    pages: number; //pagger
    count: number=1;

    view_size: number;
    pos_max: number;
    top: number=0;
    container: any;
    mouseover: boolean;



    constructor(){
        this.slides_array = [];

        //Initialisation de la config
        this.config = {
            duration: this.duration,
            autoSlide: this.autoSlide
        }


        //Assignation du tableau de slide (slide model array)
        for(let i =0; i < this.images.length; i++){
            this.slides_array[i] = new Slide(this.images[i]);
        }

        if(this.config.autoSlide){
            let interval = Observable.interval(this.config.duration).subscribe(()=>{ this.next(false) } );
            this.interval = interval;
        }

        //Initialisation du slider
        this.slides_array[this.slides_array.length -1].addClass('previous');
        this.slides_array[0].addClass("active"); 
        this.pages = this.slides_array.length;

        this.initPager();

    }


    initPager(){
        for(let i = 0; i < this.slides_array.length; i++){
            this.slides_array[i].page = i+1;
        }
    }


    //Bloque l'animation
    animatingToFalse(){
        this.animating = false;
    }

    unsubscribe(bool = false){
        if(bool){
            this.interval.unsubscribe();
            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                let interval = Observable.interval(this.duration).subscribe(()=>{ this.next(false); } );
                this.interval = interval;
            }, this.duration*1.5);
        }
    }

    stopPropagation($event){
        $event.stopPropagation();
    }

    prev(unsubscribe = false){
        
        if(!this.animating){
            this.animating = true;
            setTimeout(()=>{ this.animatingToFalse()}, <number>this.container.style.transitionDuration.replace("ms", "" ));
            this.unsubscribe(unsubscribe);
            
            if(Math.abs(this.top) <= 0){
                this.top = -this.pos_max;
                this.count = this.pages;
            }
            else{
                this.top += this.view_size;
                this.count -= 1;
            }
            this.progressBar();
        }
    }


    next(unsubscribe = false){
        if(!this.animating ){
            this.animating = true;
            setTimeout(()=>{ this.animatingToFalse()}, <number>this.container.style.transitionDuration.replace("ms", "" ));
            this.unsubscribe(unsubscribe); // désactive le slide auto pour 5 secondes
            if(Math.abs(this.top) >= this.pos_max){ 
                this.top = 0;
                this.count = 1;
            }
            else{
                this.top-= this.view_size;
                this.count += 1;
            }
        this.progressBar();
        }
    }


    progressBar(){
        this.progressBarWitdh = this.count / this.slides_array.length *100;
    }

    init(){
        this.view_size = document.querySelector(".slider").clientHeight;
        this.container = document.querySelector('.slider .container');
        this.pos_max = this.view_size * this.pages - this.view_size; // calcule de la position top max
    }


    ngAfterViewInit() {
        this.progressBar();
        this.init();
    }


    ngOnDestroy(){
        this.interval.unsubscribe();
    }
    
    onResize($event){
        this.init();
    }


}



