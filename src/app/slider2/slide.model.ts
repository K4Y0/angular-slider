import { Image } from '../interfaces/image.interface';

export class Slide implements Image{

    title: string;
    text: string;
    src : string;
    url: string;

    class_css: string;
    pager:number;
    page:number;

    constructor(slide:{title: string, text: string, src: string, url: string}){
        this.title = slide.title;
        this.text = slide.text;
        this.src = slide.src;
        this.url = slide.url;

        this.class_css = ""; 
    }

    getClass_css():Array<string>{
        return this.class_css.split(" ");
    }

    hasClass(string):boolean{
        let tab = this.class_css.split(" ");
        if(tab.indexOf(string) == -1){
            return false;
        }
        else{
            return true;
        }
    }

    addClass(string: string):string{
        this.class_css+=" "+string+" ";
        this.class_css = this.class_css.replace(/[ ]{2,}/g, ' ');
        return this.class_css;
    } 

    removeClass(string: string):string{
        this.class_css = this.class_css.replace(string, '');
        this.class_css = this.class_css.replace(/[ ]{2,}/g, ' ');
        return this.class_css;
    } 

}