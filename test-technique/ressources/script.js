

(function(){

    var $slide_img = $('.slide .content');
    var $slide = $('.slide');
    var $slider = $('.slider');

    var config = {}
    var count = 0;

    //Calcule de la taille du slider
    var calcHeight = function(i){
        var img_height = $slide_img.eq(i).height();
        $slider.height(img_height);
    }

    //Inject les flèches de navigation dans le DOM
    var injectNav = function(){
       $('<div class="nav"><span class="prev">↑</span><span class="counter"></span><span class="next">↓</span></div>').appendTo('.slider');
    }

    //compte le nombre de slide et gère la pagination
    var counter = function(){
        $('.counter').text(count+1+"/"+$slide.length);
    }

    //slide vers le bas
    var slideDown = function(){
        $slide.removeClass('previous');
        $('.slide.active').removeClass('active').addClass("previous").next('.slide').addClass('active');
        count++;
        if(count >= $slide.length){
            count = 0;
            $slide.eq(0).addClass('active');
        }
        $slider.trigger("slide");
    }

    //slide vers le haut
    var slideUp = function(){
        $slide.removeClass('previous');
        $('.slide.active').removeClass('active').addClass("previous").prev('.slide').addClass('active');
        console.log(count);
        if(count <= 0){
            count = $slide.length;
            $slide.eq(count-1).addClass('active');
        }
        count--;
        $slider.trigger("slide");
    }


    //initialise le slider et les events    
    var init = function(){

        $('.slide:first-child').addClass('active');
        injectNav();
        counter();
        calcHeight(0);
        var timeOut = setInterval(slideDown,1000); // slide every 5 seconds

        $(".next").on('click', slideDown);
        $(".prev").on('click', slideUp);
        $(window).on('resize', function(){calcHeight(count)});
        $slider.on('slide', function(){ // slide event trigger in slideDown and slideUp function
            counter();
            calcHeight(count-1);
        });
        $slider.on("mouseover", function(){ //put slider pauser if mouse enter
            clearInterval(timeOut);
        });
        $slider.on("mouseout", function(){ //slider play if mouse leave
            timeOut = setInterval(slideDown,1000);
        });

    }

    $(document).ready(init);

})();