import { SliderAngularPage } from './app.po';

describe('slider-angular App', () => {
  let page: SliderAngularPage;

  beforeEach(() => {
    page = new SliderAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
